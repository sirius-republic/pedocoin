'use strict';

/**
 * Basic coin transaction
 * @param {org.cryptol.pedonet.TransferCoins} transferCoins
 * @transaction
 */
function onTransferCoins(transferCoins) {
    if (transferCoins.amount < 0) {
        throw new Error('Negative amount');
    }
    fromWallet = transferCoins.from.wallet
    toWallet = transferCoins.to.wallet
    if (fromWallet.balance < transferCoins.amount) {
        throw new Error('Not enough coins on balance to transfer');
    }
    fromWallet.balance -= transferCoins.amount;
    toWallet.balance += transferCoins.amount;
    return getAssetRegistry('org.cryptol.pedonet.Wallet')
        .then(function(walletRegistry) {
            return walletRegistry.updateAll([fromWallet, toWallet]);
        })
}

/**
 * Maintain teachers' balance on maximum every day
 * @param {org.cryptol.pedonet.MaintainBalance} maintainBalance
 * @transaction
 */
function onMaintainBalance(maintainBalance) {
    getAssetRegistry('org.cryptol.pedonet.Wallet')
    .then(function(wr) {
        walletRegistry = wr;
        return getParticipantRegistry('org.cryptol.pedonet.Teacher')
    })
    .then(function(teacherRegistry) {
    return teacherRegistry.getAll();
    })
    .then(function(teachers) {
        for (teacher in teachers) {
            wallet = teacher.wallet
            wallet.balance = maintainBalance.maxBalance
            walletRegistry.update(wallet)
        }
    })
}

/**
 * Add users
 * @param {org.cryptol.pedonet.ExampleSetup} exampleSetup
 * @transaction
 */
function onExampleSetup(exampleSetup) {
    var factory = getFactory();
    var NS = 'org.cryptol.pedonet';

    // create 2 wallets
    var wallet1 = factory.newResource(NS, 'Wallet', '0');
    wallet1.balance = 100;
    var wallet2 = factory.newResource(NS, 'Wallet', '1');
    wallet2.balance = 200;
    var wallet3 = factory.newResource(NS, 'Wallet', '2');
    wallet3.balance = 300;

    // create the Pupil
    var pupil = factory.newResource(NS, 'Pupil', '0');
    pupil.firstName = 'John';
    pupil.lastName = 'Ivanov';
    pupil.wallet = factory.newRelationship(NS, 'Wallet', '0')

    // create the Teacher
    var teacher1 = factory.newResource(NS, 'Teacher', '1');
    teacher1.firstName = 'Inna';
    teacher1.lastName = 'Klavdievna';
    teacher1.wallet = factory.newRelationship(NS, 'Wallet', '1')
    var teacher2 = factory.newResource(NS, 'Teacher', '2');
    teacher2.firstName = 'Ovanes';
    teacher2.lastName = 'Petrovich';
    teacher2.wallet = factory.newRelationship(NS, 'Wallet', '2')

    return getAssetRegistry(NS + '.Wallet')
           .then(function(wr) {
               wr.addAll([wallet1, wallet2, wallet3]);
           })
           .then(function() {
               return getParticipantRegistry(NS + '.Pupil');
           })
           .then(function(pr) {
               pr.addAll([pupil]);
           })
           .then(function() {
               return getParticipantRegistry(NS + '.Teacher');
           })
           .then(function(pr) {
               pr.addAll([teacher1, teacher2]);
           })
}